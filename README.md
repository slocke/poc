# Gecos-HLS test tool

This page is designed to facilitate testing the Gecos-HLS tool.<br>
It allows you to run a c or cpp file through a Gecos-HLS pipeline. <br>

https://slocke.gitlabpages.inria.fr/poc/

## Running browser with no web security
For now, the only way to do http requests to the server is with a browser running no web security.
For :

**Chrome**
```
cd ~
mkdir chromeTemp
[PATH_TO_CHROME]/chrome --disable-web-security --user-data-dir=~/chromeTemp
```
You may need to close all instances of chrome before running the command

---
## Updating the jar
To update the jar located in the job running server, there are two possibilites :


### SSH : Going into a sandbox
On the AllGo website :
- Select your application
- Click on the 'Create new version' icon (top right)
- Start a sandbox
- Use SCP to transfer the new jar file to the server
- Connect to the server using SSH
- On the server, move this new jar file to ```/usr/local/bin/SHLScc.jar``` (make sure to use the correct file name) to replace the old jar
- **Commit** the sandbox

The application should now run with the new updated jar
(if you wish to modify the entrypoint of the application, the entrypoint file is located on ```/home/entrypoint```)

### Docker image
 TODO

---
## Adding more template files
To add more template c or cpp files, simply : 
- Add the file to the directory ```src/assets/CodeFiles```
- Modify ```src/assets/CodeFiles/codefiles.json``` to contain the name of the new c or cpp file. 



## Using the App
This app allows you to use GeCos-HLS by submiting a file alongside some argument, and then recuperate the results.
<br><br><kbd>![the app](images/full.png)</kbd>
#### 1 - Providing a C file
Using the interface, you can either : 
- Provide your own c/cpp code using the text editor 
- Provide your own c/cpp file using the file uploader
- Select one the the template files provided in the drop down menu
<br><br><kbd>![Input selector](images/inputs.png)</kbd>
#### 2 - Choosing arguments 
Once a c/cpp file is provided, you can now provide arguments using the text field next to the submit button
<br><br><kbd>![Arguments entry](images/arguments.png)</kbd>
<br><br> The available arguments are :
- bla :
- bla :
- bla [blo] :
#### 3 - Sending the job
By pressing the Submit button, you send the provided c/cpp file alongside the arguments specified.
Once the job is submited, the status indicator will provide you with the job status
<br><br><kbd>![Submit button](images/submit.png)</kbd><kbd>![Status field](images/status.png)</kbd>
#### 4 - Recuperate results
Once the job is completed, you are able to download any generated files, alongside a log file, output file, error file and any input file.
<br><br><kbd>![Results](images/results.png)</kbd>

