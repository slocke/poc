#include<stdio.h>
#include <ap_int.h>
#include <hls_stream.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

	
int slow(int x)
{
	#pragma HLS INLINE off
	#pragma HLS LATENCY min=1 max=1
	{
		return x * x;
	}
}

int fast(int x)
{
	{
		return x + 1;
	}
}

// Generate the Rudin-Shapiro sequence (coefficients of the Shapiro polynomials)
// cf. https://oeis.org/A020985
const static bool coeffs[64] = {
	 true, true, true,false,  true, true,false, true,  true, true, true,false, false,false, true,false, 
	 true, true, true,false,  true, true,false, true, false,false,false, true,  true, true,false, true, 
	 true, true, true,false,  true, true,false, true,  true, true, true,false, false,false, true,false,
	false,false,false, true, false,false, true,false,  true, true, true,false, false,false, true,false 
};

int rudin_shapiro(unsigned int n) {
#pragma HLS resource variable=coeffs core=RAM_1P
			return coeffs[n&0x3F]?1:-1;
}


bool mispec(int x)
{
	#pragma HLS INLINE off
	#pragma HLS LATENCY min=4 max=4
	{
		return (rudin_shapiro(x) + 1) != 2;
	}
}
		
#pragma toplevel
int single_spec_5_2(int x) {
	int i;
	i=0; 
	do
	#pragma PROFILE
	{
		{
		if (mispec(x))
			x = slow(x);
		else
			x = fast(x);
		} 
		printf("x = %d\n", x);
		i=i+1;
	} while (i<32);
	
	return x;
}

int main() {
	single_spec_5_2(12);
	return 0;
}
