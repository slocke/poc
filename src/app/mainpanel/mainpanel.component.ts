import { Component, OnInit } from '@angular/core';


interface File {
  value:String;
  viewValue:String;
}


@Component({
  selector: 'app-mainpanel',
  templateUrl: './mainpanel.component.html',
  styleUrls: ['./mainpanel.component.css']
})
export class MainpanelComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
