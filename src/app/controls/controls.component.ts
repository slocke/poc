import {Component, Injectable, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {MatSnackBar} from '@angular/material/snack-bar';
import {Observable} from "rxjs";
interface SelectFile {
  fileName:String;
  fileUrl:String;
}

interface ResultRow {
  fileName:string;
  fileUrl:string;
}

const postUrl = "https://allgo18.inria.fr/api/v1/jobs";
const token = "Token token=E5CEBGtSOMFiPBs9AItHUe4ehL5bK8GT";
const appId = "1062"
@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.css']
})
export class ControlsComponent implements OnInit {
  form = new FormControl();
  files:SelectFile[] = [
   /* {fileUrl : "simple.c", fileName : "simple.c" },
    {fileUrl :"complex.c", fileName : "complex.c" },
    {fileUrl : "multiple.c", fileName : "multiple.c" },*/
  ]

  textArea = "";
  extraArguments = "";
  uploadedFile :any = null;
  selectedFile :any = null;
  currentJobUrl:string = "null";
  currentJobId:string = "null";
  currentJobStatus:string = "null";
  resultTab:ResultRow[] = [];


  onSubmit(){
    switch (this.form.value) {
      case "1" :
        console.log("Submitting with text");
        this.submitWithText();
        break;
      case "2" :
        this.submitWithUploadedFile();
        break;

      case "3" :
        this.submitWithSelectedFile()
        console.log(this.selectedFile)
        break;
      default :
        break;

    }
    console.log("submited :" + this.form.value)
  }

  submitWithText(){
    this.doCreateJobFileRequest(new File([this.textArea],"file.cpp"))
      .subscribe(value => {
      const json = JSON.parse(JSON.stringify(value));
      this.currentJobUrl = json["url"]
      this.currentJobId = json["id"]
      this.startRefresher();
    },
        error => {
          this.snackBar.open("Error creating job","Dismiss");
        });
  }

  submitWithUploadedFile(){
    this.doCreateJobFileRequest(this.uploadedFile)
      .subscribe(value => {
        const json = JSON.parse(JSON.stringify(value));
        this.currentJobUrl = json["url"]
        this.currentJobId = json["id"]
        this.startRefresher();
      },
        error => {
          this.snackBar.open("Error creating job","Dismiss");
        });
  }

  submitWithSelectedFile(){
    this.doFileRequest(this.selectedFile).subscribe(value => {
      const fileNameSplit = (this.selectedFile as string).split("/");
      const fileName = fileNameSplit[fileNameSplit.length-1];
      const file = new File([value],fileName);
      this.doCreateJobFileRequest(file).subscribe(value => {
        const json = JSON.parse(JSON.stringify(value));
        this.currentJobUrl = json["url"]
        this.currentJobId = json["id"]
        this.startRefresher();
      },
        error => {
          this.snackBar.open("Error creating job","Dismiss");
        }
      );

    })
  /*  this.doCreateJobFileRequest(this.uploadedFile)
      .subscribe(value => {
        const json = JSON.parse(JSON.stringify(value));
        this.currentJobUrl = json["url"]
        this.currentJobId = json["id"]
        this.startRefresher();
      });*/
  }

  refreshStatus(){
    this.doGetJobStatusRequest(this.currentJobUrl)
      .subscribe(value => {
        const json = JSON.parse(JSON.stringify(value));
        this.currentJobStatus = json["status"];
        if (this.currentJobStatus == "done" || this.currentJobStatus == "error") {
          console.log(this.currentJobId);
          console.log(Object.keys(json[this.currentJobId]).length);
          for (let i of Object.keys(json[this.currentJobId])) {
            this.resultTab.push({fileName:i,fileUrl:json[this.currentJobId][i]})
          }
          console.log(this.resultTab);
          this.jobReady();
        }
      },
        error => {
          let snackBarRef = this.snackBar.open("Error getting job status","Dismiss");
          this.currentJobStatus = "error";
        })
  }

  jobReady(){
    this.stopRefresher();

  }

  onUploadFile(event:any){
    var fileList = event.target.files
    if (fileList.length > 0) {
      this.uploadedFile = fileList[0];
    }
  }


  onDownLoad(url:string,filename :string) {
    console.log("want to down :" + url)
    this.doGetJobResultRequest(url).subscribe(
      (response: any) =>{
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
        if (filename)
          downloadLink.setAttribute('download', filename);
        document.body.appendChild(downloadLink);
        downloadLink.click();
      }
    )

  }



   refreshResults(){

  }

  autoRefresh = false;
  async startRefresher() {
    this.autoRefresh = true;
    while (this.autoRefresh) {
      this.refreshStatus()
      await new Promise(f => setTimeout(f, 500));
    }
  }

  stopRefresher(){
    this.autoRefresh = false;
  }




  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.doFileListRequest().subscribe(value => {
      const json = JSON.parse(JSON.stringify(value));
      const array = json["files-array"]
      for (let item in array) {
        this.files.push({fileName:array[item],fileUrl:"assets/CodeFiles/" + array[item]})
      }
    }
   );
  }


  doCreateJobTextRequest(text : String){
    const formData: any = new FormData();
    formData.append("job[webapp_id]",appId);
    formData.append("job[param]",text);
    formData.append("job[queue]","standard");
    return this.http.post<any>(
      postUrl,
      formData,
      {
        headers : {
          "Authorization":token,
        }
      }
    );
  }

  doCreateJobFileRequest(file:File){

    const formData: any = new FormData();
    formData.append("job[webapp_id]",appId);
    formData.append("job[param]",this.extraArguments);
    formData.append("job[queue]","standard");
    formData.append("files[0]",file,file.name);
    return this.http.post<any>(
      postUrl,
      formData,
      {
        headers : {
          "Authorization":token,
        }
      }
    );
  }

  doGetJobStatusRequest(jobUrl:string){
    return this.http.get<any>(
      jobUrl,{
        headers : {
          "Authorization":token
        }
      }
    );
  }

  doGetJobResultRequest(fileUrl:string){
   /* return this.http.get<any>(
      fileUrl,{
        headers : {
          "Authorization":token
        }
      },
    );*/

    return this.http.get(fileUrl,{headers : {
        "Authorization":token
      },
      responseType: 'blob' as 'json'
    });


  }

  doFileListRequest(){
    return this.http.get("assets/CodeFiles/codefiles.json")
  }

  doFileRequest(fileUrl : string){
    console.log("R : " + fileUrl)
    return this.http.get(fileUrl, {responseType: 'blob'})
  }



  onTextAreaChange(event:any){
    this.textArea = event.target.value
  }

  onArgumentFieldChange(event:any) {
    this.extraArguments = event.target.value;
  }

}
