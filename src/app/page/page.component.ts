import {Component, Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpRequest, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { FormBuilder, FormGroup } from "@angular/forms";
@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
@Injectable()
export class PageComponent implements OnInit {
  postUrl = "https://allgo18.inria.fr/api/v1/jobs"
  getUrl = "";
  args = ""
  jobId = 0;
  constructor(
    private http: HttpClient) {
  }

  onChange(event:any){
    this.args = event.target.value
    console.log("Change to " + this.args)
  }



  postJob(): Observable<HttpResponse<Object>> {
    const formData: any = new FormData();
    formData.append("job[webapp_id]","1062");
    formData.append("job[param]",this.args);
    formData.append("job[queue]","standard");
    return this.http.post<any>(
      this.postUrl,
      formData,
      {
      headers : {
        "Authorization":"Token token=E5CEBGtSOMFiPBs9AItHUe4ehL5bK8GT",
      }
    }
    );
  }


  getJobResult() :  Observable<HttpResponse<Object>> {
    return this.http.get<any>(
      this.getUrl,{
        headers : new HttpHeaders().set( "Authorization", "Token token=E5CEBGtSOMFiPBs9AItHUe4ehL5bK8GT")
      }
    );
  }

  onAction(): void {
  //  console.log("pressed button" + "text is " + this.form.get("args"))
    //this.postJob().subscribe(value => console.log("good post"))
    console.log("doing post")
    this.postJob().subscribe(value => {
      const json = JSON.parse(JSON.stringify(value));
      this.getUrl = json["url"]
      this.jobId = json["id"]
    }, error => error)
    console.log("done post")
  }

  result = "No results"
  fileUrl = ""
  final = 0;
  onRefresh(): void{
    this.getJobResult().subscribe(value => {
      const json = JSON.parse(JSON.stringify(value));
      this.result = json["status"]
      if (this.result == "done") {
        this.fileUrl = json[this.jobId]["result.txt"];
        //this.getResultFile().subscribe(value1 => console.log(value1.text()))
        this.getResultFile();
      }
    })
  }

  getResultFile(){
    return this.http.get(
      this.fileUrl,{
        headers : {
          "Authorization":"Token token=E5CEBGtSOMFiPBs9AItHUe4ehL5bK8GT"
        }

      },
    ).subscribe(value => {this.final = value as number});
  }
  ngOnInit(): void {
  }



}
